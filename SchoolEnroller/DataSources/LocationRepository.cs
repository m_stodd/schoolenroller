﻿using SchoolEnroller.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchoolEnroller.DataSources
{
    public class LocationRepository : IEntityRepository<Location, int>
    {
        private ApplicationDbContext context;

        public LocationRepository(ApplicationDbContext context)
        {
            this.context = context;
        }

        public IQueryable<Location> GetAll()
        {
            return this.context.Locations;
        }

        public Location Add(Location entity)
        {
            throw new NotImplementedException();
        }

        public Location GetOne(int key)
        {
            return this.context.Locations.FirstOrDefault(location => location.Id == key);
        }

        public void Delete(int key)
        {
            this.context.Locations.Remove(this.GetOne(key));
            this.context.SaveChanges();
        }
    }
}