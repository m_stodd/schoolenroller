﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchoolEnroller.DataSources
{
    public interface IEntityRepository<T, Tkey>
    {
        IQueryable<T> GetAll();

        T GetOne(Tkey key);

        void Delete(Tkey key);

        T Add(T entity);
    }
}