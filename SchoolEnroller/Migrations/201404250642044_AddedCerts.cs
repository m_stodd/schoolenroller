namespace SchoolEnroller.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedCerts : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Certificates",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        DurationInDays = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Meetings", "CertificateId", c => c.Int());
            CreateIndex("dbo.Meetings", "CertificateId");
            AddForeignKey("dbo.Meetings", "CertificateId", "dbo.Certificates", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Meetings", "CertificateId", "dbo.Certificates");
            DropIndex("dbo.Meetings", new[] { "CertificateId" });
            DropColumn("dbo.Meetings", "CertificateId");
            DropTable("dbo.Certificates");
        }
    }
}
