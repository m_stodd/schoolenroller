namespace SchoolEnroller.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangedMeetingPk : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.AttendanceRecords", "MeetingId", "dbo.Meetings");
            DropPrimaryKey("dbo.Meetings");
            AlterColumn("dbo.Meetings", "Id", c => c.Int(nullable: false, identity: true));
            AddPrimaryKey("dbo.Meetings", "Id");
            AddForeignKey("dbo.AttendanceRecords", "MeetingId", "dbo.Meetings", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AttendanceRecords", "MeetingId", "dbo.Meetings");
            DropPrimaryKey("dbo.Meetings");
            AlterColumn("dbo.Meetings", "Id", c => c.Int(nullable: false));
            AddPrimaryKey("dbo.Meetings", "Id");
            AddForeignKey("dbo.AttendanceRecords", "MeetingId", "dbo.Meetings", "Id", cascadeDelete: true);
        }
    }
}
