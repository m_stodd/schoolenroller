namespace SchoolEnroller.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemovedEnrollmentDate : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Attendees", "EnrollmentDate");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Attendees", "EnrollmentDate", c => c.DateTime(nullable: false));
        }
    }
}
