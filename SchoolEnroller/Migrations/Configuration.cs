namespace SchoolEnroller.Migrations
{
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using SchoolEnroller.Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    public class DatabaseSeed
    {
        ApplicationDbContext context;

        public DatabaseSeed(ApplicationDbContext context)
        {
            this.context = context;
        }

        public void Seed()
        {
            //TODO: These roles should be enumerated
            this.context.Roles.Add(new Microsoft.AspNet.Identity.EntityFramework.IdentityRole("USER"));
            this.context.Roles.Add(new Microsoft.AspNet.Identity.EntityFramework.IdentityRole("ADMINISTRATOR"));
            this.context.Roles.Add(new Microsoft.AspNet.Identity.EntityFramework.IdentityRole("MEETING_ORGANIZER"));
            this.context.SaveChanges();

            var store = new UserStore<ApplicationUser>(this.context);
            var manager = new UserManager<ApplicationUser>(store);

            //add Mark
            var mark = new ApplicationUser()
            {
                    Email = "mark@fyin.com",
                    UserName = "mark@fyin.com"
            };
            var newIdentity = manager.Create(mark, "B!gHammer1");
            manager.AddToRole(mark.Id, "ADMINISTRATOR");

            //add Ken
            var ken = new ApplicationUser()
            {
                Email = "ken@fyin.com",
                UserName = "ken@fyin.com"
            };
            var kensIdentity = manager.Create(ken, "B!gHammer1");
            manager.AddToRole(ken.Id, "MEETING_ORGANIZER");

            this.context.Locations.AddOrUpdate(l => l.Name,
                new Location { Name = "Grand Rapids" },
                new Location { Name = "Mequon" },
                new Location { Name = "Hell" });
            this.context.SaveChanges();

            this.context.Certificates.AddOrUpdate(c => c.Name,
                new Certificate { Name = "Beard Growing", DurationInDays = 30 },
                new Certificate { Name = "Turkey Stuffing", DurationInDays = 365 },
                new Certificate { Name = "Mellon Balling", DurationInDays = 16 },
                new Certificate { Name = ".NUT Development", DurationInDays = 6 });
            this.context.SaveChanges();

            this.context.Meetings.AddOrUpdate(m => m.Title,
                new Meeting
                {
                    Title = "Growing A Beard",
                    Location = context.Locations.Where(l => l.Name == "Grand Rapids").First(),
                    Certificate = context.Certificates.Where(c => c.Name == "Beard Growing").First(),
                    Certified = true
                });
            this.context.SaveChanges();


        }
    }

    internal sealed class Configuration : DbMigrationsConfiguration<SchoolEnroller.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(SchoolEnroller.Models.ApplicationDbContext context)
        {
            new DatabaseSeed(context).Seed();

            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
        }
    }
}
