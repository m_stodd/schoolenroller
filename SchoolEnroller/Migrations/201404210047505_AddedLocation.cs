namespace SchoolEnroller.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedLocation : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Locations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Meetings", "LocationId", c => c.Int(nullable: false));
            CreateIndex("dbo.Meetings", "LocationId");
            AddForeignKey("dbo.Meetings", "LocationId", "dbo.Locations", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Meetings", "LocationId", "dbo.Locations");
            DropIndex("dbo.Meetings", new[] { "LocationId" });
            DropColumn("dbo.Meetings", "LocationId");
            DropTable("dbo.Locations");
        }
    }
}
