﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SchoolEnroller.Models
{
    public class Meeting
    {
        [JsonProperty("id")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string Title { get; set; }

        [JsonProperty("certified")]
        public bool Certified { get; set; }

        [JsonProperty("locationId")]
        public int LocationId { get; set; }

        [JsonProperty("location")]
        public virtual Location Location { get; set; }

        [JsonProperty("certificateId")]
        public int? CertificateId { get; set; }

        [JsonProperty("certificate")]
        public virtual Certificate Certificate { get; set; }

        [JsonProperty("attendanceRecords")]
        public virtual ICollection<AttendanceRecord> AttendanceRecords { get; set; }
    }
}