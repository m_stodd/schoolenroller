﻿using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchoolEnroller.Models
{
    public class User
    {
        private ApplicationUser applicationUser;
        private UserManager<ApplicationUser> userManager;

        public User(ApplicationUser applicationUser, UserManager<ApplicationUser> userManager)
        {
            this.applicationUser = applicationUser;
            this.userManager = userManager;
            
            this.Id = applicationUser.Id;
            this.Name = applicationUser.UserName;
            this.EmailAddress = applicationUser.Email;

            this.IsAdministrator = this.userManager.IsInRole(this.Id, "ADMINISTRATOR");
            this.IsMeetingOrganizer = this.userManager.IsInRole(this.Id, "MEETING_ORGANIZER");
            this.IsUser = this.userManager.IsInRole(this.Id, "USER");
        }

        public string Id { get; set; }

        public string Name { get; set; }

        public string EmailAddress { get; set; }

        public bool IsAdministrator { get; set; }

        public bool IsMeetingOrganizer { get; set; }

        public bool IsUser { get; set; }

        public void Save()
        {
            throw new NotImplementedException();
        }
    }
}