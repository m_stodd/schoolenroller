﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchoolEnroller.Models
{
    public class AttendanceRecord
    {
        public int Id { get; set; }

        public int MeetingId { get; set; }

        public virtual Meeting Meeting { get; set; }

        public int AttendeeId { get; set; }

        public virtual Attendee Attendee { get; set; }
    }
}