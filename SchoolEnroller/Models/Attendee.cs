﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SchoolEnroller.Models
{
    public class Attendee
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("pin")]
        [Required]
        public int Pin { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        public virtual ICollection<AttendanceRecord> Enrollments { get; set; }
    }
}