﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchoolEnroller.Models.WebApi
{
    public class UserApiModel
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("emailAddress")]
        public string EmailAddress { get; set; }

        [JsonProperty("isAdministrator")]
        public bool IsAdministrator { get; set; }

        [JsonProperty("isMeetingOrganizer")]
        public bool IsMeetingOrganizer { get; set; }

        [JsonProperty("isUser")]
        public bool IsUser { get; set; }
    }
}