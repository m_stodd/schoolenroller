﻿using SchoolEnroller.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SchoolEnroller.Controllers
{
    [RoutePrefix("api/attendee")]
    public class AttendeeApiController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        [HttpGet]
        [Route]
        // GET api/<controller>
        public IEnumerable<Attendee> Get()
        {
            IEnumerable<Attendee> students = this.db.Attendees.ToList();

            return students;
        }

        [HttpGet]
        [Route("{id}")]
        // GET api/<controller>/5
        public Attendee Get(int id)
        {
            if (id == null)
            {
                return null;
            }
            Attendee attendee = db.Attendees.Find(id);
            if (attendee == null)
            {
                return null;
            }
            return attendee;
        }

        [Authorize]
        [HttpPost]
        [Route]
        // POST api/<controller>
        public Attendee Post([FromBody]Attendee attendee)
        {
            Attendee created = db.Attendees.Add(attendee);
            db.SaveChanges();

            return created;
        }

        [HttpPut]
        [Route]
        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        [HttpDelete]
        [Route("{id}")]
        // DELETE api/<controller>/5
        public void Delete(int id)
        {
            Attendee attendee = db.Attendees.Find(id);
            db.Attendees.Remove(attendee);
            db.SaveChanges();
        }
    }
}