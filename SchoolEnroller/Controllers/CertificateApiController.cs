﻿using SchoolEnroller.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SchoolEnroller.Controllers
{
    [RoutePrefix("api/certificate")]
    public class CertificateApiController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        [HttpGet]
        [Route]
        // GET api/<controller>
        public IEnumerable<Certificate> Get()
        {
            IEnumerable<Certificate> certificates = this.db.Certificates.ToList();

            return certificates;
        }

        [HttpGet]
        [Route("{id}")]
        // GET api/<controller>/5
        public Certificate Get(int id)
        {
            if (id == null)
            {
                return null;
            }
            Certificate certificate = db.Certificates.Find(id);
            if (certificate == null)
            {
                return null;
            }
            return certificate;
        }

        [Authorize]
        [HttpPost]
        [Route]
        // POST api/<controller>
        public Certificate Post([FromBody]Certificate certificate)
        {
            Certificate created = db.Certificates.Add(certificate);
            db.SaveChanges();

            return created;
        }

        [HttpPut]
        [Route]
        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        [HttpDelete]
        [Route("{id}")]
        // DELETE api/<controller>/5
        public void Delete(int id)
        {
            Certificate certificate = db.Certificates.Find(id);
            db.Certificates.Remove(certificate);
            db.SaveChanges();
        }
    }
}