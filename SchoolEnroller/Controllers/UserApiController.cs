﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using SchoolEnroller.Models;
using SchoolEnroller.Models.WebApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace SchoolEnroller.Controllers
{
    [RoutePrefix("api/user")]
    public class UserApiController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        private UserManager<ApplicationUser> userManager;

        public UserApiController()
        {
            var store = new UserStore<ApplicationUser>(this.db);
            this.userManager = new UserManager<ApplicationUser>(store);
        }

        [HttpGet]
        [Route]
        // GET api/<controller>
        public IEnumerable<UserApiModel> Get()
        {
            IList<UserApiModel> apiUsers = new List<UserApiModel>();

            this.db.Users.ToList().ForEach((user) =>
                {
                    User userModel = new User(user, this.userManager);

                    apiUsers.Add(Mapper.Map<UserApiModel>(userModel));
                });

            return apiUsers;
        }

        [HttpGet]
        [Route("{id}")]
        // GET api/<controller>/5
        public UserApiModel Get(int id)
        {
            /*
            if (id == null)
            {
                return null;
            }
            Location location = db.Locations.Find(id);
            if (location == null)
            {
                return null;
            }
            return location;
             */
            throw new NotImplementedException();
        }

        [Authorize(Roles="ADMINISTRATOR")]
        [HttpPost]
        [Route]
        // POST api/<controller>
        public UserApiModel Post([FromBody]UserApiModel location)
        {
            /*
            Location created = db.Locations.Add(location);
            db.SaveChanges();

            return created;
             */
            throw new NotImplementedException();
        }

        [HttpPut]
        [Route]
        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        [Authorize(Roles = "ADMINISTRATOR")]
        [HttpDelete]
        [Route("{id}")]
        // DELETE api/<controller>/5
        public void Delete(int id)
        {
            /*
            Location location = db.Locations.Find(id);
            db.Locations.Remove(location);
            db.SaveChanges();
             */
            throw new NotImplementedException();
        }
    }
}