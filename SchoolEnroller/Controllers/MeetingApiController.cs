﻿using SchoolEnroller.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace SchoolEnroller.Controllers
{
    [RoutePrefix("api/meeting")]
    public class MeetingApiController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        [HttpGet]
        [Route]
        // GET api/<controller>
        public IEnumerable<Meeting> Get()
        {
            IEnumerable<Meeting> meeting = this.db.Meetings.ToList();

            return meeting;
        }

        [HttpGet]
        [Route("{id}")]
        // GET api/<controller>/5
        public Meeting Get(int id)
        {
            if (id == null)
            {
                return null;
            }
            Meeting meeting = db.Meetings.Find(id);
            if (meeting == null)
            {
                return null;
            }
            return meeting;
        }

        [HttpPost]
        [Route]
        // POST api/<controller>
        public Meeting Post([FromBody]Meeting meeting)
        {
            Meeting created = db.Meetings.Add(meeting);
            db.SaveChanges();

            return created;
        }

        [HttpPut]
        [Route]
        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        [HttpDelete]
        [Route("{id}")]
        // DELETE api/<controller>/5
        public void Delete(int id)
        {
            Meeting meeting = db.Meetings.Find(id);
            db.Meetings.Remove(meeting);
            db.SaveChanges();
        }
    }
}