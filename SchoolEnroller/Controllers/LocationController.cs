﻿using System.Linq;
using System.Web.Mvc;
using SchoolEnroller.Models;

namespace SchoolEnroller.Controllers
{
    public class LocationController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Student
        public ActionResult Index()
        {
            return View("IndexAngular", db.Locations.ToList());
        }
    }
}
