﻿using SchoolEnroller.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace SchoolEnroller.Controllers
{
    [RoutePrefix("api/location")]
    public class LocationApiController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        [HttpGet]
        [Route]
        // GET api/<controller>
        public IEnumerable<Location> Get()
        {
            IEnumerable<Location> students = this.db.Locations.ToList();

            return students;
        }

        [HttpGet]
        [Route("{id}")]
        // GET api/<controller>/5
        public Location Get(int id)
        {
            if (id == null)
            {
                return null;
            }
            Location location = db.Locations.Find(id);
            if (location == null)
            {
                return null;
            }
            return location;
        }

        [Authorize(Roles="ADMINISTRATOR")]
        [HttpPost]
        [Route]
        // POST api/<controller>
        public Location Post([FromBody]Location location)
        {
            Location created = db.Locations.Add(location);
            db.SaveChanges();

            return created;
        }

        [HttpPut]
        [Route]
        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        [Authorize(Roles = "ADMINISTRATOR")]
        [HttpDelete]
        [Route("{id}")]
        // DELETE api/<controller>/5
        public void Delete(int id)
        {
            Location location = db.Locations.Find(id);
            db.Locations.Remove(location);
            db.SaveChanges();
        }
    }
}