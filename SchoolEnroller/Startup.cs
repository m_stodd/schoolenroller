﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SchoolEnroller.Startup))]
namespace SchoolEnroller
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
