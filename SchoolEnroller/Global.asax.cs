﻿using SchoolEnroller.Models;
using SchoolEnroller.Models.WebApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace SchoolEnroller
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            this.ConfigureAutomapper();

            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        private void ConfigureAutomapper()
        {
            AutoMapper.Mapper.CreateMap<User, UserApiModel>();
            AutoMapper.Mapper.CreateMap<UserApiModel, User>();
        }
    }
}
