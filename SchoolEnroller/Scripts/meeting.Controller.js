﻿var meetingController = function ($scope, $http, loginService, locationService, certificateService, meetingService) {

    $scope.currentError = {}

    locationService.getLocations().
        success(function (data, status, headers, config) {
            $scope.locations = data;
        }).
        error(function (data, status, headers, config) {
            $scope.currentError = data
            // called asynchronously if an error occurs
            // or server returns response with an error status.
        });

    $scope.meeting = new Object();

    $scope.getCertificates = function () {
        certificateService.getCertificates().
        success(function (data, status, headers, config) {
            $scope.certificates = data;
        }).
        error(function (data, status, headers, config) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
        });
    }

    $scope.getCertificates();
    
    $scope.addMeeting = function (meeting) {
        if (meeting.certificate == null) {
            meeting.certificateId = null
        } else {
            meeting.certificateId = meeting.certificate.id;
        }
        
        meetingService.addMeeting(
            {
                    name: meeting.name,
                    locationId: meeting.location.id,
                    certificateId: meeting.certificateId
                }).
        success(function (data, status, headers, config) {
            $scope.getMeetings();
            $scope.meeting = {};
        }).
        error(function (data, status, headers, config) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
        });
    }

    $scope.getMeetings = function () {
        meetingService.getMeetings().
        success(function (data, status, headers, config) {
            $scope.meetings = data;
            // this callback will be called asynchronously
            // when the response is available
        }).
        error(function (data, status, headers, config) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
        });
    }

    $scope.getMeetings();

    $scope.deleteMeeting = function (id) {
        meetingService.deleteMeeting(id).
        success(function (data, status, headers, config) {
            $scope.getMeetings();
            $scope.meeting = {}
            // this callback will be called asynchronously
            // when the response is available
        }).
        error(function (data, status, headers, config) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
        });
    }
}

meetingController.$inject = ['$scope', '$http', 'loginService', 'locationService', 'certificateService', 'meetingService']
attendanceTrackerApp.controller('meetingController', meetingController);