﻿var loginController = function ($scope, $http, loginService) {
    $scope.loggedIn = loginService.loggedIn;

    $scope.login = function () {
        loginService.login()
        $scope.loggedIn = loginService.loggedIn
    }
}

loginController.$inject = ['$scope', '$http', 'loginService']
attendanceTrackerApp.controller('loginController', loginController)