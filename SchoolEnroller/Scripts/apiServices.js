﻿//Location Service
var certificateService = function ($http) {
    var certService = Object.create($http)

    certService.getCertificates = function () {
        return this.get('/api/certificate')
    }

    certService.addCertificate = function (certificate) {
        return this.post('/api/certificate', certificate);
    }

    certService.deleteCertificate = function (certId) {
        return this.delete('/api/certificate/' + certId);
    }

    return certService;
}
certificateService.$inject = ['$http']
attendanceTrackerApp.factory('certificateService', certificateService);


//Location Service
var locationService = function ($http) {
    var locationService = Object.create($http)

    locationService.getLocations = function () {
        return this.get('/api/location');
    }

    locationService.addLocation = function (location) {
        return this.post('api/location', location);
    }

    locationService.deleteLocation = function (locationId) {
        return this.delete('/api/location/' + locationId);
    }

    return locationService;
}
locationService.$inject = ['$http']
attendanceTrackerApp.factory('locationService', locationService);

var meetingService = function ($http) {
    var meetingService = Object.create($http)

    meetingService.getMeetings = function () {
        return this.get('/api/meeting');
    }

    meetingService.addMeeting = function (meeting) {
        return this.post('api/meeting', meeting);
    }

    meetingService.deleteMeeting = function (meetingId) {
        return this.delete('/api/meeting/' + meetingId);
    }

    return meetingService;
}
meetingService.$inject = ['$http']
attendanceTrackerApp.factory('meetingService', meetingService);

var userService = function ($http) {
    var userService = Object.create($http)

    
    userService.getUsers = function () {
        
        /*
        var createUser = function (userId, email, isAdmin, isMeetingOgranizer, isUser) {
            return {
                userId: userId,
                emailAddress: email,
                isAdministrator: isAdmin,
                isMeetingOrganizer: isMeetingOgranizer,
                isUser: isUser
            }
        }

        var users = []
        users.push(createUser(1, 'mark@fyin.com', true, false, false))
        users.push(createUser(2, 'ken@fyin.com', false, true, false))
        users.push(createUser(3, 'qing@fyin.com', false, false, true))

        return users
        */

        return this.get('/api/user');
    }

    userService.updateUsers = function (users) {
        alert(users);
        //return this.put('api/attendee', attendee);
    }

    userService.deleteUser = function (userId) {
        return this.delete('/api/user/' + userId);
    }

    return userService;
}
userService.$inject = ['$http']
attendanceTrackerApp.factory('userService', userService);

var attendeeService = function ($http) {
    var attendeeService = Object.create($http)

    attendeeService.getAttendees = function () {
        return this.get('/api/attendee');
    }

    attendeeService.addAttendee = function (attendee) {
        return this.post('api/attendee', attendee);
    }

    attendeeService.deleteAttendee = function (attendeeId) {
        return this.delete('/api/attendee/' + attendeeId);
    }

    return attendeeService;
}
attendeeService.$inject = ['$http']
attendanceTrackerApp.factory('attendeeService', attendeeService);