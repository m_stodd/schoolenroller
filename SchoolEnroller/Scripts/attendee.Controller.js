﻿var attendeeController = function ($scope, attendeeService) {

    $scope.attendee = new Object();

    $scope.addAttendee = function (attendee) {
        attendeeService.addAttendee(attendee).
        success(function (data, status, headers, config) {
            $scope.getAttendees();
            $scope.attendee = new Object();
        }).
        error(function (data, status, headers, config) {
            alert(status);
            // called asynchronously if an error occurs
            // or server returns response with an error status.
        });
    }

    $scope.getAttendees = function () {
        attendeeService.getAttendees().
        success(function (data, status, headers, config) {
            $scope.data = data;
            // this callback will be called asynchronously
            // when the response is available
        }).
        error(function (data, status, headers, config) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
        });
    }

    $scope.getAttendees();

    $scope.deleteAttendee = function (id) {
        attendeeService.deleteAttendee(id).
        success(function (data, status, headers, config) {
            $scope.getAttendees();
            // this callback will be called asynchronously
            // when the response is available
        }).
        error(function (data, status, headers, config) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
        });
    }
}

attendeeController.$inject = ['$scope', 'attendeeService']
attendanceTrackerApp.controller('attendeeController', attendeeController);