﻿
var locationController = function ($scope, locationService) {

    $scope.location = new Object();
    
    $scope.addLocation = function (location) {
        locationService.addLocation(location).
        success(function (data, status, headers, config) {
            $scope.getLocations();
            $scope.location = new Object();
        }).
        error(function (data, status, headers, config) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
        });
    }

    $scope.getLocations = function () {
        locationService.getLocations().
        success(function (data, status, headers, config) {
            $scope.data = data;
            // this callback will be called asynchronously
            // when the response is available
        }).
        error(function (data, status, headers, config) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
        });
    }

    $scope.getLocations();

    $scope.deleteLocation = function (id) {
        locationService.deleteLocation(id).
        success(function (data, status, headers, config) {
            $scope.getLocations();
            // this callback will be called asynchronously
            // when the response is available
        }).
        error(function (data, status, headers, config) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
        });
    }
}

locationController.$inject = ['$scope', 'locationService']
attendanceTrackerApp.controller("locationController", locationController);