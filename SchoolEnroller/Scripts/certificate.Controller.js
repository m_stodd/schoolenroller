﻿var certificateController = function ($scope, certificateService) {

    $scope.certificate = new Object();

    $scope.addCertificate = function (certificate) {
        certificateService.addCertificate(certificate).
        success(function (data, status, headers, config) {
            $scope.getCertificates();
            $scope.certificate = new Object();
        }).
        error(function (data, status, headers, config) {
            alert(status);
            // called asynchronously if an error occurs
            // or server returns response with an error status.
        });
    }

    $scope.getCertificates = function () {
        certificateService.getCertificates().
        success(function (data, status, headers, config) {
            $scope.certificates = data;
        }).
        error(function (data, status, headers, config) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
        });
    }

    $scope.getCertificates();

    $scope.deleteCertificate = function (id) {
        certificateService.deleteCertificate(id).
        success(function (data, status, headers, config) {
            $scope.getCertificates();
        }).
        error(function (data, status, headers, config) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
        });
    }
}

certificateController.$inject = ['$scope', 'certificateService']
attendanceTrackerApp.controller('certificateController', certificateController);