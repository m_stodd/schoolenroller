﻿var userController = function ($scope, userService) {
    /*
    $scope.addAttendee = function (attendee) {
        attendeeService.addAttendee(attendee).
        success(function (data, status, headers, config) {
            $scope.getAttendees();
            $scope.attendee = new Object();
        }).
        error(function (data, status, headers, config) {
            alert(status);
            // called asynchronously if an error occurs
            // or server returns response with an error status.
        });
    }
    */

    $scope.updateUsers = function (users) {
        userService.updateUsers(users);
        $scope.getUsers();
    }

    $scope.getUsers = function () {
        userService.getUsers().
        success(function (data, status, headers, config) {
            $scope.users = data;
            // this callback will be called asynchronously
            // when the response is available
        }).
        error(function (data, status, headers, config) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
        });
    }

    $scope.getUsers();

    $scope.deleteUser = function (id) {
        userService.deleteUser(id).
        success(function (data, status, headers, config) {
            $scope.getUsers();
            // this callback will be called asynchronously
            // when the response is available
        }).
        error(function (data, status, headers, config) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
        });
    }
}

userController.$inject = ['$scope', 'userService']
attendanceTrackerApp.controller('userController', userController);