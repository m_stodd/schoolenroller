﻿'use strict';
/* App Module */
var attendanceTrackerApp = angular.module('attendanceTrackerApp', []);

//Login Service
var loginService = function ($http) {
    var shinyNewServiceInstance = new function () { }

    shinyNewServiceInstance.loggedIn = false;

    shinyNewServiceInstance.login = function () {
        this.loggedIn = true;
    }
    //factory function body that constructs shinyNewServiceInstance
    return shinyNewServiceInstance;
}

loginService.$inject = ['$http']
attendanceTrackerApp.factory('loginService', loginService);